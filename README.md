# Gitlab Helpers
Some helper templates for the GitLab CI/CD to avoid writing duplicate code.

## How to use
You can include the files directly from GitLab, example is below. To 
see more options, please refer to the [gitlab-ci.yml reference](https://docs.gitlab.com/ee/ci/yaml/#include)

```yaml
include:
- project: 'mireiawenrose/gitlab-helpers'
  ref: 'master'
  file:
  - '/workflow-mr.yml'
  - '/kaniko.yml'
```

## Workflow: Merge Requests
This workflow tries to avoid duplicate pipelines when merge request is 
open in branch. This is taken from 
[GitLab documentation](https://docs.gitlab.com/ee/ci/yaml/workflow.html#switch-between-branch-pipelines-and-merge-request-pipelines)

This is included in `/workflow-mr.yml` -file.

## Kaniko
Kaniko image building and pushing helper. This is taken from 
[GitLab documentation](https://docs.gitlab.com/ee/ci/docker/using_kaniko.html)

### Template: `.kaniko:build`
This template builds and pushes the Docker image.

#### Example
```yaml
build:image:
  stage: 'build'
  extends: '.kamiko:build'
  variables:
    TAG: 'latest'
```

#### Settings
The build uses GitLab default package repository by default.

`DOCKERFILE`
The Dockerfile to build. Defaults to `Dockerfile`, but may need to be 
changed if multiple images are built or Dockerfile name is changed.

`TARGET`
The Dockerfile target to build.

`TAG` 
The tag to build, defaults to `${CI_COMMIT_TAG}` which may not be set.

`CI_REGISTRY_IMAGE`
The registry image to push to, without the tag. For example 
`registry.tld/project/image`

`NOPUSH`
Set to any value to disable pushing to the registry. Defaults to unset. 
For example `NOPUSH=1`

`TARBALL`
A tarball filename to write the image to, if set. Defaults to unset.

`CACHE`
Cache timeout in hours. Setting this enables caching. Defaults to 
unset.

For example; `15m` or `1h`

`CACHE_REPO`
Set this flag to specify a remote repository that will be used to 
store cached layers. Defaults to being unset.

If this flag is not provided, a cache repo will be inferred from the 
`CI_REGISTRY_IMAGE`. For example, using `registry.tld/project/image`, 
then cached layers will be stored in `registry.tld/project/image/cache`.

`CI_REGISTRY`, `CI_CACHE_REGISTRY`, 
`CI_REGISTRY_USERNAME`, `CI_CACHE_REGISTRY_USERNAME`,
`CI_REGISTRY_PASSWORD`, `CI_CACHE_REGISTRY_PASSWORD`
Credentials for given registry. If cache registry is same as normal registry, 
it is not needed to define it.

`KANIKO_IMAGE`
The Kaniko image to use, defaults to latest debug executor.

`CI_PROJECT_DIR`
The working directory for the project, if not set tries `/workspace` as default.
