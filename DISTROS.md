# Available Linux distribution images
This uses images by Geerlingguy, and the list is taken from their GitHub repositories.

## Run matrixes
- `molecule:test`
  - Runs test matrix with all of the supported images
- `molecule:test:apt`
  - Runs test matrix with all of the images marked as part of APT matrix
- `molecule:test:rpm`
  - Runs test matrix with all of the images marked as part of RPM matrix

Note: the matrixes are updated by hand.

## Amazon Linux
Included in `RPM` run matrix.

[EoL information](https://endoflife.date/amazon-linux)

- `amazonlinux2`
- `amazonlinux2023`

## CentOS
CentOS is dicontinued and not included in the run matrixes.

[EoL information](https://endoflife.date/centos)

- [EOL] `centos6`
- [EOL] `centos7`
- [EOL] `centos8`

## Debian
End of life versions are not included in the run matrixes. Versions in LTS support only are considered end of life.

Included in APT run matrix.

[EoL information](https://endoflife.date/debian)

- [EOL] `debian8`
- [EOL] `debian9`
- [EOL] `debian10`
- `debian11`
- `debian12`

## Fedora Linux
End of life versions are not included in the run matrixes.

Included in `RPM` run matrix.

[EoL information](https://endoflife.date/fedora)

- [EOL] `fedora27`
- [EOL] `fedora29`
- [EOL] `fedora30`
- [EOL] `fedora31`
- [EOL] `fedora32`
- [EOL] `fedora33`
- [EOL] `fedora34`
- [EOL] `fedora35`
- [EOL] `fedora36`
- `fedora37`
- `fedora38`

## OpenSUSE Leap
End of life versions are not included in the run matrixes.

Included in `RPM` run matrix.

[EoL information](https://endoflife.date/opensuse)

- `opensuseleap15`

## Rocky Linux
End of life versions are not included in the run matrixes.

Included in `RPM` run matrix.

[EoL information](https://endoflife.date/rocky-linux)

- `rockylinux8`
- `rockylinux9`

## Red Hat Universal Base Image
End of life versions are not included in the run matrixes.

Included in `RPM` run matrix.

[EoL information](https://endoflife.date/rhel)

- `ubi8`

## Ubuntu
End of life versions are not included in the run matrixes. Versions in extended security support only are considered end of life.

Included in APT run matrix.

[EoL information](https://endoflife.date/ubuntu)

- [EOL] `ubuntu1604`
- [EOL] `ubuntu1804`
- [EOL] `ubuntu2004`
- `ubuntu2204`
